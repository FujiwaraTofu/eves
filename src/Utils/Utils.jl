module Utils

import LinearAlgebra

la = LinearAlgebra

# check that all elements of the input array are equal
# returns true for an empty input array
all_equal(arr::Array{<:Any}) = begin
    v1 = arr[1]
    return all(map((v) -> v == v1, arr))
end

all_apprx(arr::Array{<:Any}) = begin
    v1 = arr[1]
    return all(map((v) -> v ≈ v1, arr))
end

# minimum number of iters required to perform a linear regression on difference
# norms
MIN_REQ_ITERS_LIN = 3

# given an iterated quantity, estimate the spectral radius
# drop_front / drop_back : percentage of iterated collection to drop at the
# start and end for global spectral radius estimation
est_rho(iqc; order = 2, drop_front = 0.25, drop_back = 0.25) = begin
    # remove iqc iterated values that are to be dropped from the front and back
    n_iters = length(iqc)
    n_drop_front = Int(ceil(n_iters * drop_front))
    n_drop_back = Int(ceil(n_iters * drop_back))

    n_use = (n_iters - n_drop_front - n_drop_back)

    if n_use < MIN_REQ_ITERS_LIN
        error("insufficient number of iterations")
    end

    iqc_use = iqc[(n_drop_front + 1):(end - n_drop_back)]

    n_diff_norms_use = n_use - 1
    diff_norms_use = [la.norm(iqc_use[i+1] - iqc_use[i], order) for i in 1:n_diff_norms_use]

    slope, inter = [1:n_diff_norms_use ones(n_diff_norms_use)] \ log.(diff_norms_use)

    return exp(slope)
end

# provide iteration-to-iteration spectral radius estimates
iter_rhos(iqc; order = 2) = begin
    n_iters      = length(iqc)
    n_diff_norms = n_iters - 1
    n_est_rhos   = n_diff_norms - 1
    diff_norms = [la.norm(iqc[i+1] - iqc[i], order) for i in 1:n_diff_norms]
    est_rhos   = [diff_norms[i+1] / diff_norms[i] for i in 1:n_est_rhos]
    return est_rhos
end

end # module Utils
