module Types
# export some helpful type aliases

Fl   = Float64
Fl64 = Float64
Fl32 = Float32
export Fl, Fl64, Fl32

Arr = Array
Vec = Vector
Mat = Matrix
export Arr, Vec, Mat

VFl = Vector{Fl}
VIn = Vector{Int}
export VFl, VIn

Fun = Function
export Fun

end # module Types
