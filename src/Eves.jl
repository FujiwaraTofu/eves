# TODO : expand Eves module hierarchy
module Eves

# include sub-modules
include("Types/Types.jl") # type aliases
include("Utils/Utils.jl") # utility functions

using  .Types # introduce type aliases (Fl, Arr, ...)
import .Utils

# external packages
import LinearAlgebra
import OffsetArrays
import SparseArrays

# module name aliases (to de-clutter)
ut = Utils
la = LinearAlgebra
oa = OffsetArrays
sp = SparseArrays

# # NOTE : quadrature set generation

# generate LQN angular quadrature points and weights in the principal octant (μ,
# η, ξ > 0). weights are normalized to sum to 4π over the unit-sphere.
include("Quad/LQN.jl") # pre-generated LQN ordinates / weights
gen_lqn_pr(N::Int) = begin
    @assert MIN_LQN_ORDER <= N <= MAX_LQN_ORDER # disallow neg. weights
    @assert iseven(N)

    lqn_set = lqn_sets[N] # from Quad/LQN.jl

    pjs   = lqn_set[:projs]
    indws = lqn_set[:weights]
    inds  = lqn_set[:inds]

    No = div(N * (N+2), 8) # no. ordinates per octant

    μs_pr = Arr{Fl,1}(undef, No) # (pr)incipal octant; μ > 0
    ηs_pr = Arr{Fl,1}(undef, No) # ...                 η > 0
    ξs_pr = Arr{Fl,1}(undef, No) # ...                 ξ > 0

    ws_pr = Arr{Fl,1}(undef, No)

    m = 1
    for i in 1:(Int(N / 2))
        for j in 1:(Int(N / 2) - i + 1)
            μ = pjs[i]
            η = pjs[j]
            ξ = sqrt(1.0 - μ^2 - η^2)
            μs_pr[m] = μ
            ηs_pr[m] = η
            ξs_pr[m] = ξ

            # normalized to 4π over the unit-sphere
            ws_pr[m] = (π / 2) * indws[inds[m]]

            m += 1
        end
    end

    return μs_pr, ηs_pr, ξs_pr, ws_pr
end

# # NOTE : enums referring to segments of the unit-sphere

# NOTE : we use the following space, direction, and orientation conventions
# x-axis: directed along +/- i (right / left )
# y-axis: directed along +/- j (up    / down )
# z-axis: directed along +/- k (out   / in   )

# octant
@enum OC_ID begin
    OC_ID_RUO  = 1 # (R)i - (U)p - (O)u ; (+i, +j, +k) (principal)
    OC_ID_LUO      # (L)e - (U)p - (O)u ; (-i, +j, +k)
    OC_ID_RDO      # (R)i - (D)o - (O)u ; (+i, -j, +k)
    OC_ID_LDO      # (L)e - (D)o - (O)u ; (-i, -j, +k)
    OC_ID_RUI      # (R)i - (U)p - (I)n ; (+i, +j, -k)
    OC_ID_LUI      # (L)e - (U)p - (I)n ; (-i, +j, -k)
    OC_ID_RDI      # (R)i - (D)o - (I)n ; (+i, -j, -k)
    OC_ID_LDI      # (L)e - (D)o - (I)n ; (-i, -j, -k)
end
# principal (μ, η, ξ > 0)
PR_OC_ID   = OC_ID_RUO
# standard octant collection covering the unit-sphere
STD_OC_IDS = [OC_ID_RUO, OC_ID_LUO, OC_ID_RDO, OC_ID_LDO,
              OC_ID_RUI, OC_ID_LUI, OC_ID_RDI, OC_ID_LDI]

# quadrant (TODO : add remaining quadrants, e.g., RO, LI, ...)
@enum QC_ID begin
    QC_ID_RU = 1 # (R)i - (U)p ; (+i; +j) (principal)
    QC_ID_LU     # (L)e - (U)p ; (-i; +j)
    QC_ID_RD     # (R)i - (D)o ; (+i, -j)
    QC_ID_LD     # (L)e - (D)o ; (-i, -j)
end
STD_QC_IDS = [QC_ID_RU, QC_ID_LU, QC_ID_RD, QC_ID_LD]

# hemisphere (TODO : add remaining hemispheres, e.g., U, D, O, ...)
@enum HC_ID begin
    HC_ID_R = 1 # (R)i ; (+i) ; (principal)
    HC_ID_L     # (L)e ; (-i)
end
STD_HC_IDS = [HC_ID_R, HC_ID_L]

# AngColl's are used to index into AngQuad's
abstract type AngColl end
AnCo = AngColl

# refers to angles belonging to a given unit-sphere octant
struct OctaColl <: AngColl
    oc_id :: OC_ID
end
OcCo = OctaColl

# defining constant values
PR_OC  = OcCo(PR_OC_ID) # principal OctaColl
OC_RUO = OcCo(OC_ID_RUO)
OC_LUO = OcCo(OC_ID_LUO)
OC_RDO = OcCo(OC_ID_RDO)
OC_LDO = OcCo(OC_ID_LDO)
OC_RUI = OcCo(OC_ID_RUI)
OC_LUI = OcCo(OC_ID_LUI)
OC_RDI = OcCo(OC_ID_RDI)
OC_LDI = OcCo(OC_ID_LDI)
STD_OCS = OcCo.(STD_OC_IDS)

# check orientation
is_ruo(oc::OcCo) = (oc.oc_id == OC_ID_RUO)
is_luo(oc::OcCo) = (oc.oc_id == OC_ID_LUO)
is_rdo(oc::OcCo) = (oc.oc_id == OC_ID_RDO)
is_ldo(oc::OcCo) = (oc.oc_id == OC_ID_LDO)
is_rui(oc::OcCo) = (oc.oc_id == OC_ID_RUI)
is_lui(oc::OcCo) = (oc.oc_id == OC_ID_LUI)
is_rdi(oc::OcCo) = (oc.oc_id == OC_ID_RDI)
is_ldi(oc::OcCo) = (oc.oc_id == OC_ID_LDI)

is_ru(oc::OcCo) = (is_ruo(oc) || is_rui(oc))
is_lu(oc::OcCo) = (is_luo(oc) || is_lui(oc))
is_rd(oc::OcCo) = (is_rdo(oc) || is_rdi(oc))
is_ld(oc::OcCo) = (is_ldo(oc) || is_ldi(oc))

is_r(oc::OcCo) = (is_ruo(oc) || is_rdo(oc) || is_rui(oc) || is_rdi(oc))
is_l(oc::OcCo) = (is_luo(oc) || is_ldo(oc) || is_lui(oc) || is_ldi(oc))
is_u(oc::OcCo) = (is_ruo(oc) || is_luo(oc) || is_rui(oc) || is_lui(oc))
is_d(oc::OcCo) = (is_rdo(oc) || is_ldo(oc) || is_rdi(oc) || is_ldi(oc))
is_o(oc::OcCo) = (is_ruo(oc) || is_luo(oc) || is_rdo(oc) || is_ldo(oc))
is_i(oc::OcCo) = (is_rui(oc) || is_lui(oc) || is_rdi(oc) || is_ldi(oc))

# check the sign of angular cosines
μ_si(oc::OcCo) = begin
    if     is_r(oc)
        si = + 1
    elseif is_l(oc)
        si = - 1
    end

    return si
end
η_si(oc::OcCo) = begin
    if     is_u(oc)
        si = + 1
    elseif is_d(oc)
        si = - 1
    end

    return si
end
ξ_si(oc::OcCo) = begin
    if     is_o(oc)
        si = + 1
    elseif is_i(oc)
        si = - 1
    end

    return si
end

# refers to angles belonging to a given unit-sphere quadrant
struct QuadColl <: AngColl
    qc_id :: QC_ID
end
QuCo = QuadColl

QC_RU = QuCo(QC_ID_RU)
QC_LU = QuCo(QC_ID_LU)
QC_RD = QuCo(QC_ID_RD)
QC_LD = QuCo(QC_ID_LD)
STD_QCS = QuCo.(STD_QC_IDS)

is_ru(qc::QuCo) = (qc.qc_id == QC_ID_RU)
is_lu(qc::QuCo) = (qc.qc_id == QC_ID_LU)
is_rd(qc::QuCo) = (qc.qc_id == QC_ID_RD)
is_ld(qc::QuCo) = (qc.qc_id == QC_ID_LD)

is_r(qc::QuCo) = (is_ru(qc) || is_rd(qc))
is_l(qc::QuCo) = (is_lu(qc) || is_ld(qc))
is_u(qc::QuCo) = (is_ru(qc) || is_lu(qc))
is_d(qc::QuCo) = (is_rd(qc) || is_ld(qc))

μ_si(qc::QuCo) = begin
    if     is_r(qc)
        si = + 1
    elseif is_l(qc)
        si = - 1
    end

    return si
end
η_si(qc::QuCo) = begin
    if     is_u(qc)
        si = + 1
    elseif is_d(qc)
        si = - 1
    end

    return si
end

# split a QuCo into its component OcCo's
# two octants make up a quadrant
split_QuCo_OcCo(qc::QuCo) = begin
    qc_id = qc.qc_id
    if     qc_id == QC_ID_RU
        ocs = [OC_RUO, OC_RUI]
    elseif qc_id == QC_ID_LU
        ocs = [OC_LUO, OC_LUI]
    elseif qc_id == QC_ID_RD
        ocs = [OC_RDO, OC_RDI]
    elseif qc_id == QC_ID_LD
        ocs = [OC_LDO, OC_LDI]
    end

    return ocs
end

# angular quadrature
abstract type AngQuad end
AnQu = AngQuad

# symmetric quadrature (i.e. reflected ordinates over x-y, x-z, y-z)

# determines octant ordering for how SyQu's μs, ηs, ... are laid out
SQ_OCS = STD_OCS
struct SymmQuad <: AngQuad
    nm :: Int       # no. ordinates (over the unit sphere)
    μs :: Arr{Fl,1} # $\hat{i}$-comp. (nm × 1)
    ηs :: Arr{Fl,1} # $\hat{j}$-comp. (nm × 1)
    ξs :: Arr{Fl,1} # $\hat{k}$-comp. (nm × 1)
    ws :: Arr{Fl,1} # weight  (nm × 1)

    # accepts (pr)incipal octant values (μ, η, ξ > 0)
    SymmQuad(μs_pr, ηs_pr, ξs_pr, ws_pr) = begin
        @assert ut.all_equal(length.([μs_pr, ηs_pr, ξs_pr, ws_pr]))

        μs = vcat([μ_si(oc) * μs_pr for oc in SQ_OCS]...)
        ηs = vcat([η_si(oc) * ηs_pr for oc in SQ_OCS]...)
        ξs = vcat([ξ_si(oc) * ξs_pr for oc in SQ_OCS]...)
        ws = vcat([           ws_pr for oc in SQ_OCS]...)

        nm = length(ws)

        return new(nm, μs, ηs, ξs, ws)
    end
end
SyQu = SymmQuad

# no. ordinates / sphere
get_nm(sq::SyQu) = sq.nm
# no. ordinates / octant
get_nm_oc(sq::SyQu) = div(sq.nm, 8)

# octant m-index offset
get_oc_off(sq::SyQu, oc::OcCo) = begin
    oc_idx  = findfirst((sq_oc) -> oc == sq_oc, SQ_OCS)
    return (oc_idx - 1)*get_nm_oc(sq)
end

# ordinate indices for an octant
get_ms(sq::SyQu, oc::OcCo) = begin
    oc_off = get_oc_off(sq, oc)
    ms = oc_off .+ (1:get_nm_oc(sq))
    return ms
end

# ordinate indices for a quadrant
get_ms(sq::SyQu, qc::QuCo) = vcat([get_ms(sq, oc) for oc in split_QuCo_OcCo(qc)]...)

# spatial mesh
abstract type Mesh end

# rectangular block mesh
# TODO : linear indexing
struct RectMesh <: Mesh
    ni   :: Int       # no. cells along $\hat{i}$
    nj   :: Int       # ...             $\hat{j}$
    Δxs  :: Arr{Fl,2} # (ni × nj) (NOTE : unnec. storage, since $Δx_{i,j} = Δx_{i} ∀ j$)
    Δys  :: Arr{Fl,2} # (ni × nj)  ...                          $Δy_{i,j} = Δy_{j} ∀ i$)
end
ReMe = RectMesh

# ReMe constructor
# i_Δxs (ni) : cell-width  (along $\hat{i}$)
# j_Δys (nj) : cell-height (along $\hat{j}$)
RectMesh(i_Δxs::Arr{Fl,1}, j_Δys::Arr{Fl,1}) = begin
    ni = length(i_Δxs)
    nj = length(j_Δys)

    Δxs = repeat(i_Δxs, outer=(1,nj))
    Δys = repeat(reshape(j_Δys, 1, nj), outer=(ni,1))

    return RectMesh(ni, nj, Δxs, Δys)
end

# uniform ReMe constructor
RectMesh(ni::Int, nj::Int, Δx::Fl, Δy::Fl) = begin
    Δxs = fill(Δx, ni, nj)
    Δys = fill(Δy, ni, nj)

    return RectMesh(ni, nj, Δxs, Δys)
end

# get cell-edge coordinates (ordered tuple, x (left, right), y (bottom, top))
get_ces(rm::ReMe, i::Int, j::Int) = begin
    xl = sum(rm.Δxs[1:i-1, j])
    xr = xl + rm.Δxs[i,j]

    yb = sum(rm.Δys[i, 1:j-1])
    yt = yb + rm.Δys[i, j]

    return xl, xr, yb, yt
end

# cell-center (x)-values
get_xcs(rm::ReMe) = begin
    xcs = Array{Fl,2}(undef, rm.ni, rm.nj)
    for i in 1:rm.ni
        xl, xr, _, _ = get_ces(rm, i, 1)
        xcs[i, :] .= (xl + xr) / 2
    end
    return xcs
end
# ...         (y)-values
get_ycs(rm::ReMe) = begin
    ycs = Array{Fl,2}(undef, rm.ni, rm.nj)
    for j in 1:rm.nj
        _, _, yb, yt = get_ces(rm, 1, j)
        ycs[:, j] .= (yb + yt) / 2
    end
    return ycs
end

# i index containing x
get_i(rm::ReMe, x::Fl) = begin
    sΔxs = rm.Δxs[:,1] # identical across j
    exs = cumsum([0.; sΔxs]) # edge x-values

    X = exs[end]
    if (x ≤ 0.) || (X ≤ x)
        error()
    end

    i = findfirst(exs .> x) - 1
    return i
end
# j index containing y
get_j(rm::ReMe, y::Fl) = begin
    sΔys = rm.Δys[1,:] # identical across i
    eys = cumsum([0.; sΔys]) # edge y-values

    Y = eys[end]
    if (y ≤ 0.) || (Y ≤ y)
        error()
    end

    j = findfirst(eys .> y) - 1
    return j
end
# i,j indices containing x,y
get_ij(rm::ReMe, x::Fl, y::Fl) = begin
    i = get_i(rm, x)
    j = get_j(rm, y)
    return i, j
end

# (R)ectMesh (B)oundaries
@enum RB begin
    RB_L = 1
    RB_R
    RB_B
    RB_T
end
STD_RBS = [RB_L, RB_R, RB_B, RB_T]

# are the provided indices adjacent to a (R)ectMesh (B)oundary?
is_adj_rb_l(rm::ReMe, i::Int, j::Int) = (i == 1)     # left?
is_adj_rb_r(rm::ReMe, i::Int, j::Int) = (i == rm.ni) # right?
is_adj_rb_b(rm::ReMe, i::Int, j::Int) = (j == 1)     # bottom?
is_adj_rb_t(rm::ReMe, i::Int, j::Int) = (j == rm.nj) # top?
is_adj_rb(rm::ReMe, i::Int, j::Int) = begin          # any of the above?
    return (   is_adj_rb_l(rm, i, j)
            || is_adj_rb_r(rm, i, j)
            || is_adj_rb_b(rm, i, j)
            || is_adj_rb_t(rm, i, j))
end

# grab (b)oundary-(a)djacent-(c)ell indice(s) (returns a tuple, (is, js))
get_bacs_rb_l(rm::ReMe) = fill(1, rm.nj), 1:rm.nj      # (l)eft
get_bacs_rb_r(rm::ReMe) = fill(rm.ni, rm.nj), 1:rm.nj  # (r)ight
get_bacs_rb_b(rm::ReMe) = 1:rm.ni, fill(1, rm.ni)      # (b)ottom
get_bacs_rb_t(rm::ReMe) = 1:rm.ni, fill(rm.nj, rm.ni)  # (t)op
# b.a.c's on the specified (b)oundary (f)ace
get_bacs(rm::ReMe, rb::RB) = begin
    if     (rb == RB_L)
        bacs = get_bacs_rb_l(rm)
    elseif (rb == RB_R)
        bacs = get_bacs_rb_r(rm)
    elseif (rb == RB_B)
        bacs = get_bacs_rb_b(rm)
    elseif (rb == RB_T)
        bacs = get_bacs_rb_t(rm)
    end

    return bacs
end

# i, j offsets into boundary-adjacent (gh)ost (c)ells
get_ghc_offs(rm::ReMe, rb::RB) = begin
    if     (rb == RB_L)
        gio, gjo = -1,  0
    elseif (rb == RB_R)
        gio, gjo = +1,  0
    elseif (rb == RB_B)
        gio, gjo =  0, -1
    elseif (rb == RB_T)
        gio, gjo =  0, +1
    end

    return gio, gjo
end

# (inc)ident QuadColls for a RectMesh boundary
get_inc_qcs(rm::ReMe, rb::RB) = begin
    if     (rb == RB_L)
        inc_qcs = [QC_RU, QC_RD]
    elseif (rb == RB_R)
        inc_qcs = [QC_LU, QC_LD]
    elseif (rb == RB_B)
        inc_qcs = [QC_RU, QC_LU]
    elseif (rb == RB_T)
        inc_qcs = [QC_RD, QC_LD]
    end
    return inc_qcs
end
# (out)going QuadColls for a RectMesh boundary
get_out_qcs(rm::ReMe, rb::RB) = begin
    if     (rb == RB_L)
        out_qcs = [QC_LU, QC_LD]
    elseif (rb == RB_R)
        out_qcs = [QC_RU, QC_RD]
    elseif (rb == RB_B)
        out_qcs = [QC_RD, QC_LD]
    elseif (rb == RB_T)
        out_qcs = [QC_RU, QC_LU]
    end
    return out_qcs
end

# grab the (incoming or outgoing) (re)flected QuadColl on a RectMesh boundary
get_ref_qc(rm::ReMe, qc::QuCo, rb::RB) = begin
    if     (rb == RB_L) || (rb == RB_R)
        if     is_ru(qc)
            re_qc = QC_LU
        elseif is_lu(qc)
            re_qc = QC_RU
        elseif is_rd(qc)
            re_qc = QC_LD
        elseif is_ld(qc)
            re_qc = QC_RD
        end
    elseif (rb == RB_B) || (rb == RB_T)
        if     is_ru(qc)
            re_qc = QC_RD
        elseif is_lu(qc)
            re_qc = QC_LD
        elseif is_rd(qc)
            re_qc = QC_RU
        elseif is_ld(qc)
            re_qc = QC_LU
        end
    end

    return re_qc
end

# background material (cross sections / opacities)
struct Material
    rm   :: ReMe      # mesh
    σts  :: Arr{Fl,2} # total      (mesh.ni × mesh.nj)
    σas  :: Arr{Fl,2} # absorption (mesh.ni × mesh.nj)
end
Mat = Material

# uniform material constructor
Material(rm::ReMe, σt::Fl, σa::Fl) = begin
    σts = fill(σt, rm.ni, rm.nj)
    σas = fill(σa, rm.ni, rm.nj)
    return Material(rm, σts, σas)
end

abstract type TransportDisc end

# TODO : generalize (right now, BLD-specific)
struct DFE_Transport <: TransportDisc
    nb :: Int # no. basis/weight funcs. per cell

    # volume-integration
    m  :: Arr{Fl,2} # (m)ass
    gx :: Arr{Fl,2} # (g)radient (x)-component
    gy :: Arr{Fl,2} # ...        (y)-component

    # surface-integration
    sx :: Arr{Fl,2} # (s)urface (x)-component
    sy :: Arr{Fl,2} # ...       (y)-component

    # (s)urface (g)etter (m)atrices
    l_sgm :: Arr{Fl,2} # left
    r_sgm :: Arr{Fl,2} # right
    b_sgm :: Arr{Fl,2} # bottom
    t_sgm :: Arr{Fl,2} # top

    # basis cell-adjacency functions
    get_x_adj :: Fun # i-adjacent cells (sep. w/ $\hat{n} = \pm \hat{i}$)
    get_y_adj :: Fun # j-adjacent cells (sep. w/ $\hat{n} = \pm \hat{j}$)
end
DFE_Tr = DFE_Transport

# grab (u)p(w)ind and (d)own(w)ind surface-getter-matrices for a QuCo (x-dir.)
get_sx_sgms(dtr::DFE_Tr, qc::QuCo) = begin
    return dw, uw = is_r(qc) ? (dtr.r_sgm, dtr.l_sgm) : (dtr.l_sgm, dtr.r_sgm)
end
# ... (y-dir.)
get_sy_sgms(dtr::DFE_Tr, qc::QuCo) = begin
    return dw, uw = is_u(qc) ? (dtr.t_sgm, dtr.b_sgm) : (dtr.b_sgm, dtr.t_sgm)
end

abstract type DiffusionDisc end

# NOTE : specific to Adams' ``mostly consistent'' BLD diffusion discretization
# TODO : generalize
struct DFE_Diffusion <: DiffusionDisc
    nb :: Int # no. basis/weight funcs. per cell

    # volume-integration
    m  :: Arr{Fl,2} # (m)ass
    gx :: Arr{Fl,2} # (g)radient (x)-component
    gy :: Arr{Fl,2} # ...        (y)-component

    # surface-integration
    ifl :: Arr{Fl,2} # (i)nterior surface (f)lux, (l)eft
    ifb :: Arr{Fl,2} # ...                        (b)ottom
    ifr :: Arr{Fl,2} # ...                        (r)ight
    ift :: Arr{Fl,2} # ...                        (t)op

    efl :: Arr{Fl,2} # (e)xterior surface (f)lux, (l)eft
    efb :: Arr{Fl,2} # ...                        (b)ottom
    efr :: Arr{Fl,2} # ...                        (r)ight
    eft :: Arr{Fl,2} # ...                        (t)op

    icl :: Arr{Fl,2} # (i)nterior surface (c)urrent, (l)eft
    icb :: Arr{Fl,2} # ...                           (b)ottom
    icr :: Arr{Fl,2} # ...                           (r)ight
    ict :: Arr{Fl,2} # ...                           (t)op

    ecl :: Arr{Fl,2} # (e)xterior surface (c)urrent, (l)eft
    ecb :: Arr{Fl,2} # ...                           (b)ottom
    ecr :: Arr{Fl,2} # ...                           (r)ight
    ect :: Arr{Fl,2} # ...                           (t)op
end
DFE_Di = DFE_Diffusion

# march cell (i,j) for ordinates within QuadColl qc in-place mutation of ψs!
function cell_march!(aq::AnQu, mat::Mat, rm::ReMe, dtr::DFE_Tr,
                     qs, ϕs, ψs,
                     qc::QuCo, i::Int, j::Int)
    # grab downwind and upwind surface getter matrices
    dw_sx_sgm, uw_sx_sgm = get_sx_sgms(dtr, qc)
    dw_sy_sgm, uw_sy_sgm = get_sy_sgms(dtr, qc)

    # downwind surface matrices
    dw_sxm = dtr.sx * dw_sx_sgm
    dw_sym = dtr.sy * dw_sy_sgm

    # upwind surface matrices
    uw_sxm = dtr.sx * uw_sx_sgm
    uw_sym = dtr.sy * uw_sy_sgm

    # adjacent cell basis indices
    x_adj_idxs = dtr.get_x_adj(i, j)
    y_adj_idxs = dtr.get_y_adj(i, j)

    # loop over ordinates in the QuadColl
    for m in get_ms(aq, qc)
        mlhs = (  aq.μs[m]*rm.Δys[i,j]*(dw_sxm + dtr.gx)
                + aq.ηs[m]*rm.Δxs[i,j]*(dw_sym + dtr.gy)
                + mat.σts[i,j]*rm.Δxs[i,j]*rm.Δys[i,j]*dtr.m)

        # grab adjacent fluxes (i.e. outside cell)
        ψs_x_adj = [ψs[b, m, i, j] for (b, i, j) in x_adj_idxs]
        ψs_y_adj = [ψs[b, m, i, j] for (b, i, j) in y_adj_idxs]

        mrhs = (- aq.μs[m]*rm.Δys[i,j]*uw_sxm*ψs_x_adj
                - aq.ηs[m]*rm.Δxs[i,j]*uw_sym*ψs_y_adj
                + (  (1 / (4*π))*rm.Δxs[i,j]*rm.Δys[i,j]*dtr.m
                   * ((mat.σts[i,j] - mat.σas[i,j])*ϕs[:,i,j] + qs[:,i,j])))

        ψs[:,m,i,j] = mlhs \ mrhs
    end

    return nothing # ψs mutated in-place
end

# generate the march ordering
gen_march_order(aq::AnQu, rm::ReMe, qc::QuCo) = begin
    di_is = is_r(qc) ? (1:rm.ni) : (is_l(qc) ? (rm.ni:-1:1) : error())
    di_js = is_u(qc) ? (1:rm.nj) : (is_d(qc) ? (rm.nj:-1:1) : error())

    return repeat(di_is, outer=rm.nj), repeat(di_js, inner=rm.ni)
end

# boundary condition specification
# TODO : generalize to arb. meshes, include periodic
struct BoundSpec
    nre    :: Int # no. refl. boundaries
    rbs_re :: Array{RB,1} # refl. boundaries
    BoundSpec(rbs_re) = begin
        nre = length(rbs_re)
        # duplication forbidden
        @assert length(unique(rbs_re)) == nre
        return new(nre, rbs_re)
    end
end
BoSp = BoundSpec

# determine the order in which QuadColl's should be marched in a sweep (qcmo)
# NOTE : throws for b.c.'s requiring special care (e.g., no. refl. >= 3)
# NOTE : returned sub-arrays may be marched simultaneously
gen_qcmo(bs::BoSp) = begin
    if     bs.nre == 0
        qcmo = [STD_QCS]
    elseif bs.nre == 1
        rb = bs.rbs_re[1]

        if     (rb == RB_L)
            qcmo = [[QC_LU, QC_LD], [QC_RU, QC_RD]]
        elseif (rb == RB_R)
            qcmo = [[QC_RU, QC_RD], [QC_LU, QC_LD]]
        elseif (rb == RB_B)
            qcmo = [[QC_RD, QC_LD], [QC_RU, QC_LU]]
        elseif (rb == RB_T)
            qcmo = [[QC_RU, QC_LU], [QC_RD, QC_LD]]
        end
    elseif bs.nre == 2
        srbs = sort(bs.rbs_re)

        if     srbs == sort([RB_L, RB_B])
            qcmo = [[QC_LD], [QC_LU, QC_RD], [QC_RU]]
        elseif srbs == sort([RB_B, RB_R])
            qcmo = [[QC_RD], [QC_RU, QC_LD], [QC_LU]]
        elseif srbs == sort([RB_R, RB_T])
            qcmo = [[QC_RU], [QC_LU, QC_RD], [QC_LD]]
        elseif srbs == sort([RB_T, RB_L])
            qcmo = [[QC_LU], [QC_RU, QC_LD], [QC_RD]]
        else
            error()
        end
    else
        # TEMP : replace error() throw by a default ordering
        # error()
        # NOTE : in this case, a single round of sweeping through each QuCo
        # CANNOT "solve" the SI fixed-source problem, the reflective boundaries
        # require multiple sweeps to converge.
        qcmo = [STD_QCS]
    end

    return qcmo
end

# get corresponding outgoing / reflected (ghost cell) basis indices on a
# boundary
# TODO : this isn't very general..., too specific to BLD
# NOTE : one-to-one correspondence between out_bs and ref_bs
get_ref_bs(rm::ReMe, dtr::DFE_Tr, rb::RB) = begin
    if     (rb == RB_L)
        out_bs, ref_bs = [1, 4], [2, 3]
    elseif (rb == RB_B)
        out_bs, ref_bs = [1, 2], [4, 3]
    elseif (rb == RB_R)
        out_bs, ref_bs = [2, 3], [1, 4]
    elseif (rb == RB_T)
        out_bs, ref_bs = [3, 4], [2, 1]
    else
        error()
    end

    return out_bs, ref_bs
end

boundary_update!(aq::AnQu, rm::ReMe, dtr::DFE_Tr, bsp::BoSp, ψs) = begin
    # reflective
    for rb in bsp.rbs_re
        out_qcs = get_out_qcs(rm, rb)
        ref_qcs = [get_ref_qc(rm, qc, rb) for qc in out_qcs]

        # outgoing and reflected ordinate indices
        out_ms = vcat([get_ms(aq, qc) for qc in out_qcs]...)
        ref_ms = vcat([get_ms(aq, qc) for qc in ref_qcs]...)

        # outgoing and reflected basis indices
        out_bs, ref_bs = get_ref_bs(rm, dtr, rb)

        # boundary cell indices
        bis, bjs = get_bacs(rm, rb)

        # adjacent ghost-cell offsets
        gio, gjo = get_ghc_offs(rm, rb)

        for (i,j) in zip(bis, bjs)
            gi = i + gio
            gj = j + gjo

            ψs[ref_bs, ref_ms, gi, gj] = ψs[out_bs, out_ms, i, j]
        end
    end
    # TODO : periodic, etc.
    return nothing # NOTE : in-place mutation of ψs
end


gen_dsa_sys(ddi::DFE_Di, aq::AnQu, ma::Mat, rm::ReMe, bs::BoSp) = begin
    nb, ni, nj = ddi.nb, rm.ni, rm.nj

    lixs = LinearIndices((nb,ni,nj)) # linear index mapping
    nrm  = ncm = length(lixs)        # row/column length

    lhsm = sp.spzeros(nrm, ncm) # left-hand-side matrix
    rhsm = sp.spzeros(nrm, ncm) # right-hand-side matrix

    ms_rh = [get_ms(aq, QC_RU); get_ms(aq, QC_RD)] # (r)ight-(h)emisphere
    μs_rh, ws_rh = aq.μs[ms_rh], aq.ws[ms_rh]
    α = (1 / (4*π))*sum(μs_rh .* ws_rh) # ≈ (1/4) (angular quadrature error)

    for j in 1:nj
        for i in 1:ni
            # linear indices for (c)urrent (c)ell
            cc_lis = lixs[1:nb, i, j]

            # standard (d)iffusion (c)oefficient
            dc = 1 / (3 * ma.σts[i,j])

            # reduce clutter
            σa = ma.σas[i,j]
            σs = ma.σts[i,j] - ma.σas[i,j]
            Δx = rm.Δxs[i,j]
            Δy = rm.Δys[i,j]

            # interior (within-cell)
            # volume-integration
            cc_lvim = dc*((Δy / Δx)*ddi.gx + (Δx / Δy)*ddi.gy) + σa*Δx*Δy*ddi.m
            cc_rvim = σs*Δx*Δy*ddi.m

            # surface-integration
            con_cc_ifsi = zeros(nb, nb) # (con)tribution (doesn't have α scaling)
            con_cc_icsi = zeros(nb, nb) # ...            (... $D$ ...)

            # NOTE : on a reflective boundary, interior/exterior surface terms cancel
            if ! (is_adj_rb_l(rm, i, j) && (RB_L in bs.rbs_re))
                con_cc_ifsi += Δy*ddi.ifl
                con_cc_icsi += (Δy / Δx)*ddi.icl
            end
            if ! (is_adj_rb_r(rm, i, j) && (RB_R in bs.rbs_re))
                con_cc_ifsi += Δy*ddi.ifr
                con_cc_icsi += (Δy / Δx)*ddi.icr
            end
            if ! (is_adj_rb_b(rm, i, j) && (RB_B in bs.rbs_re))
                con_cc_ifsi += Δx*ddi.ifb
                con_cc_icsi += (Δx / Δy)*ddi.icb
            end
            if ! (is_adj_rb_t(rm, i, j) && (RB_T in bs.rbs_re))
                con_cc_ifsi += Δx*ddi.ift
                con_cc_icsi += (Δx / Δy)*ddi.ict
            end

            # don't forget the scaling factors!
            cc_ifsi = α * con_cc_ifsi
            cc_icsi = (- dc / 2) * con_cc_icsi

            lhsm[cc_lis, cc_lis] += cc_lvim + cc_ifsi + cc_icsi
            rhsm[cc_lis, cc_lis] += cc_rvim

            # exterior (without-cell)
            # NOTE : for boundary faces, no vacuum contrib., refl. cancels
            if ! is_adj_rb_l(rm, i, j)
                lc_lis = lixs[1:nb, i-1, j  ]

                lc_Δx = rm.Δxs[i-1, j]
                lc_σt = ma.σts[i-1, j]
                lc_dc = 1 / (3*lc_σt)

                lc_efsi = -α*Δy*ddi.efl
                lc_ecsi = (- lc_dc / 2)*(Δy / lc_Δx)*ddi.ecl
                lhsm[cc_lis, lc_lis] += lc_efsi + lc_ecsi
            end
            if ! is_adj_rb_b(rm, i, j)
                bc_lis = lixs[1:nb, i  , j-1]

                bc_Δy = rm.Δys[i, j-1]
                bc_σt = ma.σts[i, j-1]
                bc_dc = 1 / (3*bc_σt)

                bc_efsi = -α*Δx*ddi.efb
                bc_ecsi = (- bc_dc / 2)*(Δx / bc_Δy)*ddi.ecb
                lhsm[cc_lis, bc_lis] += bc_efsi + bc_ecsi
            end
            if ! is_adj_rb_r(rm, i, j)
                rc_lis = lixs[1:nb, i+1, j  ]

                rc_Δx = rm.Δxs[i+1, j]
                rc_σt = ma.σts[i+1, j]
                rc_dc = 1 / (3*rc_σt)

                rc_efsi = -α*Δy*ddi.efr
                rc_ecsi = (- rc_dc / 2)*(Δy / rc_Δx)*ddi.ecr
                lhsm[cc_lis, rc_lis] += rc_efsi + rc_ecsi
            end
            if ! is_adj_rb_t(rm, i, j)
                tc_lis = lixs[1:nb, i  , j+1]

                tc_Δy = rm.Δys[i, j+1]
                tc_σt = ma.σts[i, j+1]
                tc_dc = 1 / (3*tc_σt)

                tc_efsi = -α*Δx*ddi.eft
                tc_ecsi = (- tc_dc / 2)*(Δx / tc_Δy)*ddi.ect
                lhsm[cc_lis, tc_lis] += tc_efsi + tc_ecsi
            end
        end
    end

    return lhsm, rhsm
end

# default iteration named-parameter values
de_mi   = Inf
de_ϵ    = 1.e-8
de_cf   = (o, n) -> la.norm(n - o, Inf) < de_ϵ
de_sv   = true
de_difr = 5
de_di   = (i) -> nothing
# de_di   = (i) -> (i % de_difr == 0) ? println("i = $(i)") : nothing

# # NOTE : iteration functions
# fpf  : (f)ixed-(p)oint-(f)unction, i.e. we're iterating evaluations of this
# vs   : iterated vector
# es   : extra state information carried through iteration
# mi   : max no. iterations
# cf   : convergence function
# save : flag, save iterative vs
# disp : display (progress) function

# standard, unaccelerated (f)ixed-(p)oint-(i)teration
function fpi(fpf::Fun, vs_i::Vec{Fl}, es_i::Any;
             mi   :: Number = de_mi,
             cf   :: Fun    = de_cf,
             save :: Bool   = de_sv,
             disp :: Fun    = de_di)
    @assert (mi >= 0)

    # set initial estimates
    vs_p = vs_i
    es_p = es_i

    # initialize save storage
    vss = []

    conv = false # flag, have we converged?
    i = 0        # no. iterations completed

    # don't iterative if ...
    (mi == 0) ? (@goto lab_mi) : nothing
    while true
        # evaluate display function
        disp(i)

        # run a single evaluation of our (f)ixed-(p)oint-(f)unction
        vs_n, es_n = fpf(vs_p, es_p)

        # if requested, save iterated vs
        if save
            push!(vss, vs_n)
        end

        conv = cf(vs_p, vs_n) # check convergence
        i += 1                # increment iteration count

        # carry over to the next iteration
        vs_p = vs_n
        es_p = es_n

        # jump out of the loop if we've converged or reached max iter. crit.
        conv      ? (@goto lab_cf) : nothing
        (i == mi) ? (@goto lab_mi) : nothing
    end
    @label lab_mi # goto's have a time and a place, this is one of them
    @label lab_cf

    return conv, vs_p, es_p, vss
end

# DMD update given snapshot matrices
function dmd_update((Y_minus, Y_plus)::Tuple{Arr{Fl,2}, Arr{Fl,2}},
                    (v_Lp1, v_Lp2)::Tuple{Vec{Fl}, Vec{Fl}})
    # thin SVD
    svd_obj = la.svd(Y_minus; full = false)
    U  = svd_obj.U
    # NOTE : Julia uses A' for the complex conjugate of A
    Ut = U'
    S  = la.Diagonal(svd_obj.S)
    V  = svd_obj.V

    A_approx = Ut * Y_plus * V * inv(S)
    Δz = (la.I - A_approx) \ (Ut * (v_Lp2 - v_Lp1))

    return v_Lp1 + U*Δz
end

# DMD update given matrix formed from iterated vectors
function dmd_update(vM::Arr{Fl,2})
    m, n = size(vM)
    L = n - 2

    # NOTE : Julia arrays are indexed starting at 1 :(
    Y_minus = vM[:, 2:(L + 1)] - vM[:, 1:L]
    Y_plus  = vM[:, 3:(L + 2)] - vM[:, 2:(L + 1)]

    v_Lp1 = vM[:, L + 1]
    v_Lp2 = vM[:, L + 2]

    return dmd_update((Y_minus, Y_plus), (v_Lp1, v_Lp2))
end

moot_coarsen = (f_vs::VFl) -> f_vs                     # doesn't coarsen
moot_interpo = (_::VFl, _::VFl, c_vs_n::VFl) -> c_vs_n # doesn't interpolate

# coarsening / interpolation defaults
de_co, de_in = moot_coarsen, moot_interpo

# (d)ynamic (m)ode (d)ecomposition accelerated fixed-point-iteration (with coarsening)
# R : no. smoothing iterations (before snapshot matrix construction)
# L : snapshot matrix ($Y_{-}$ and $Y_{+}$) no. columns
function dmd(fpf::Fun, R::Int, L::Int, vs_i::Vec{Fl}, es_i::Any;
             coarsen :: Fun    = de_co,
             interpo :: Fun    = de_in,
             mi      :: Number = de_mi,
             cf      :: Fun    = de_cf,
             save    :: Bool   = de_sv,
             disp    :: Fun    = de_di)

    @assert (mi >= 0)
    @assert (R  >= 0)
    @assert (L  >= 1)

    # (r)unning (p)revious
    vs_rp = vs_i
    es_rp = es_i

    vss = []

    conv = false
    i = 0 # no. (f)ixed-(p)oint-(f)unction evaluations

    (mi == 0) ? (@goto lab_mi) : nothing
    while true
        # smoothing
        for r in 1:R
            disp(i)
            vs_rn, es_rn = fpf(vs_rp, es_rp)

            if save
                push!(vss, vs_rn)
            end

            conv = cf(vs_rp, vs_rn)
            i += 1

            vs_rp = vs_rn
            es_rp = es_rn

            conv      ? (@goto lab_cf) : nothing
            (i == mi) ? (@goto lab_mi) : nothing
        end

        # data collection (coarsened)
        c_vs_rp = coarsen(vs_rp)
        cl = length(c_vs_rp)

        c_vm = Matrix{Fl}(undef, cl, L + 2)
        c_vm[:, 1] = c_vs_rp

        for l in 1:(L+1)
            disp(i)
            vs_rn, es_rn = fpf(vs_rp, es_rp)
            c_vs_rn = coarsen(vs_rn)

            c_vm[:, l + 1] = c_vs_rn

            if save
                push!(vss, vs_rn)
            end

            conv = cf(vs_rp, vs_rn)
            i += 1

            vs_rp   = vs_rn
            es_rp   = es_rn
            c_vs_rp = c_vs_rn

            conv      ? (@goto lab_cf) : nothing
            (i == mi) ? (@goto lab_mi) : nothing
        end

        # DMD update
        c_vs_rn = dmd_update(c_vm)
        vs_rn   = interpo(vs_rp, c_vs_rp, c_vs_rn)

        # carry-over
        vs_rp = vs_rn
    end
    @label lab_mi
    @label lab_cf

    return conv, vs_rp, es_rp, vss
end

# (and)erson accelerated fixed-point-iteration (with coarsening)
# TODO : optimize (especially for memory usage)
# TODO : this code is extraordinarily ugly, a rewrite is desperately needed!
# m : anderson storage length
function and(fpf::Fun, m::Int, v_i::VFl, e_i::Any;
             coarsen :: Fun    = de_co,
             interpo :: Fun    = de_in,
             mi      :: Number = de_mi,
             cf      :: Fun    = de_cf,
             save    :: Bool   = de_sv,
             disp    :: Fun    = de_di)
    @assert (mi >= 0)
    @assert (m  >  0)

    v_rp = v_i
    e_rp = e_i

    vs = []

    conv = false
    i = 0 # no. iterations completed (fpf)

    (mi == 0) ? (@goto lab_mi) : nothing

    # NOTE : Setup
    disp(i)
    v_rn, e_rn = fpf(v_rp, e_rp)

    c_0 = coarsen(v_rp)
    c_1 = coarsen(v_rn)
    nc = length(c_0)

    cs = [c_0 c_1]
    gs = reshape(c_1 - c_0, nc, 1)

    if save
        push!(vs, v_rn)
    end

    conv = cf(v_rp, v_rn)
    i += 1

    v_rp = v_rn
    e_rp = e_rn

    conv      ? (@goto lab_cf) : nothing
    (i == mi) ? (@goto lab_mi) : nothing

    k = 1
    while true
        mk = min(m, k)

        ck = cs[:, end]

        disp(i)
        v_rn_ua, e_rn = fpf(v_rp, e_rp)

        c_rn_ua = coarsen(v_rn_ua)
        gk = c_rn_ua - ck

        gs = [gs gk]

        sg = gs[:, (k-mk+2):end] - gs[:, (k-mk+1):(end-1)]
        sc = cs[:, (k-mk+2):end] - cs[:, (k-mk+1):(end-1)]

        γk = sg \ gk

        cn = ck + gk - (sc + sg)*γk

        # use the new coarse- anderson update to interpolate for vs_rp
        v_rn = interpo(v_rn_ua, c_rn_ua, cn)

        if save
            push!(vs, v_rn)
        end

        conv = cf(v_rp, v_rn)
        i += 1

        v_rp   = v_rn
        e_rp   = e_rn

        conv      ? (@goto lab_cf) : nothing
        (i == mi) ? (@goto lab_mi) : nothing

        cs = [cs cn]

        k = k + 1
    end
    @label lab_mi
    @label lab_cf

    return conv, v_rp, e_rp, vs
end

# shift between data representations
sh_v2ϕ(rm::ReMe, dtr::DFE_Tr, vs::VFl)       = reshape(vs, dtr.nb, rm.ni, rm.nj)
sh_ϕ2v(rm::ReMe, dtr::DFE_Tr, ϕs::Arr{Fl,3}) = reshape(ϕs, dtr.nb * rm.ni * rm.nj)

gen_sh_v2ϕ(rm::ReMe, dtr::DFE_Tr) = ((vs::VFl)       -> sh_v2ϕ(rm, dtr, vs))
gen_sh_ϕ2v(rm::ReMe, dtr::DFE_Tr) = ((ϕs::Arr{Fl,3}) -> sh_ϕ2v(rm, dtr, ϕs))

# calculate scalar flux from angular flux
calc_ϕs(aq::AnQu, rm::ReMe, dtr::DFE_Tr, ψs) = begin
    nb, nm, ni, nj = dtr.nb, aq.nm, rm.ni, rm.nj
    ϕs = mapslices((ψ) -> sum(ψ .* aq.ws), ψs, dims = (2))[1:nb,1,1:ni,1:nj]
    return oa.no_offset_view(ϕs)
end

# transport acceleration procedures (e.g., SI, DSA, NDA, QD, etc.)
@enum ACC begin
    ACC_SI  = 1 # (S)ource (I)teration
    ACC_DSA     # (D)iffusion (S)ynthetic (A)cceleration
    # TODO ...
end

# include finite-element matrices, etc.
include("DFE/DFE.jl")
# TEMP
import .DFE.BLD
import .DFE.BLD.Std.DSA

# pre-define some BLD DFE_Tr variants
# standard BLD
dtr_bld_std = DFE_Tr(BLD.nb,
                     BLD.Std.m, BLD.Std.gx, BLD.Std.gy,
                     BLD.Std.sx, BLD.Std.sy,
                     BLD.l_sgm, BLD.r_sgm, BLD.b_sgm, BLD.t_sgm,
                     BLD.get_x_adj, BLD.get_y_adj)
# mass-lumped BLD
dtr_bld_mlu = DFE_Tr(BLD.nb,
                     BLD.Lum.m, BLD.Std.gx, BLD.Std.gy,
                     BLD.Std.sx, BLD.Std.sy,
                     BLD.l_sgm, BLD.r_sgm, BLD.b_sgm, BLD.t_sgm,
                     BLD.get_x_adj, BLD.get_y_adj)

# fully-lumped BLD
dtr_bld_lum = DFE_Tr(BLD.nb,
                     BLD.Lum.m, BLD.Lum.gx, BLD.Lum.gy,
                     BLD.Lum.sx, BLD.Lum.sy,
                     BLD.l_sgm, BLD.r_sgm, BLD.b_sgm, BLD.t_sgm,
                     BLD.get_x_adj, BLD.get_y_adj)

# Adams' semi-consistent diffusion discretization (for the unlumped case)
ddi_asc_std = DFE_Di(BLD.nb,
                     DSA.m, DSA.gx, DSA.gy,
                     DSA.ifl, DSA.ifb, DSA.ifr, DSA.ift,
                     DSA.efl, DSA.efb, DSA.efr, DSA.eft,
                     DSA.icl, DSA.icb, DSA.icr, DSA.ict,
                     DSA.ecl, DSA.ecb, DSA.ecr, DSA.ect)

de_dtr = dtr_bld_std
de_ddi = ddi_asc_std

# generate a fixed-source outer-iteration fixed-point-function (SI or DSA)
# aq  : angular quadrature
# ma  : material (cross sections and opacities)
# rm  : spatial mesh
# bsp : boundary condition specification
# acc : transport acceleration method
# nts : no. of transport sweeps to perform (boundary condition convergence)
function gen_outer_fpf(aq::AnQu, ma::Mat, rm::ReMe, bsp::BoSp, qs;
                       dtr  :: DFE_Tr = de_dtr,
                       ddi  :: DFE_Di = de_ddi,
                       acc :: ACC  = ACC_DSA,
                       nts :: Int  = 1)
    # TEMP, TODO (dangerous) : assume linear transport i.e., fixed DSA matrices
    if acc == ACC_DSA
        dsa_lhs, dsa_rhs = gen_dsa_sys(ddi, aq, ma, rm, bsp)
    end

    function outer_fpf(vs_i, es_i)
        # iter. not.: (i)nput, (p)revious, (t)ransport, (n)ext, (o)utput

        # phase-space dimensions
        nb = dtr.nb
        nm = aq.nm
        ni = rm.ni
        nj = rm.nj

        # unpack (reshape) arguments
        ϕs_p = reshape(vs_i, nb, ni, nj)
        # TODO : this deepcopy is an efficiency nightmare
        ψs_p = deepcopy(es_i) # NOTE : prevent in-place mutation!

        # QuCo march ordering
        qcmo = gen_qcmo(bsp)

        for _ in 1:nts # boundary condition convergence
            # TEMP : for now, QuCo's are not marched in parallel
            for qc in vcat(qcmo...)
                is_mo, js_mo = gen_march_order(aq, rm, qc)
                for (i, j) in zip(is_mo, js_mo)
                    cell_march!(aq, ma, rm, dtr, qs, ϕs_p, ψs_p, qc, i, j)
                end
                boundary_update!(aq, rm, dtr, bsp, ψs_p)
            end
        end
        ψs_t = ψs_p # NOTE : in-place modification by cell_march! and boundary_update!
        ϕs_t = calc_ϕs(aq, rm, dtr, ψs_t)

        if     acc == ACC_SI # (S)ource (I)teration
            ψs_n = ψs_t
            ϕs_n = ϕs_t
        elseif acc == ACC_DSA # (D)iffusion (S)ynthetic (A)cceleration
            ψs_n = ψs_t # unaccelerated

            # compute the scalar flux difference,
            vdϕs = reshape(ϕs_t - ϕs_p, nb*ni*nj)
            # solve the DSA problem,
            vfs = dsa_lhs \ (dsa_rhs * vdϕs)
            # reshape the correction vector,
            fs_n = reshape(vfs, nb, ni, nj)
            # add the DSA correction to the transport flux,
            ϕs_n = ϕs_t + fs_n
        else
            error()
        end

        # pack (reshape) outputs
        vs_o = reshape(ϕs_n, nb*ni*nj)
        es_o = ψs_n

        return vs_o, es_o
    end

    return outer_fpf
end

# generate a function to represent the soln.
gen_sol_rep(rm::ReMe, dtr::DFE_Tr, ϕs::Array{Fl,3}) = begin
    sol_rep(x::Fl, y::Fl) = begin
        # what cell are we in?
        i, j = get_ij(rm, x, y)

        # get edge coordinates
        xl, xr, yb, yt = get_ces(rm, i, j)

        # grab cell width and height
        Δx, Δy = rm.Δxs[i,j], rm.Δys[i,j]

        # grab the finite element basis values for cell (i,j)
        bs = ϕs[:,i,j]

        # contributions from each (cardinal) basis function
        c1 = bs[1] * (xr - x )*(yt - y ) / (Δx*Δy)
        c2 = bs[2] * (x  - xl)*(yt - y ) / (Δx*Δy)
        c3 = bs[3] * (x  - xl)*(y  - yb) / (Δx*Δy)
        c4 = bs[4] * (xr - x )*(y  - yb) / (Δx*Δy)

        ϕ = c1 + c2 + c3 + c4

        return ϕ
    end

    return sol_rep
end

end # module Eves
