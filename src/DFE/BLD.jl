# (B)i(l)inear (D)iscontinuous on (R)ectangles
module BLD

import LinearAlgebra
la = LinearAlgebra

nb = 4 # no. basis/weight funcs. per cell

# (s)urface-(g)etter (m)atrices for (r)ectangles
# multiplying an (m,i,j)'s basis flux vector ($\psi_{k}$), by a surface's SGM
# removes (sets to zero) values not lying on that surface.
l_sgm = la.diagm([1., 0., 0., 1.]) # (L)eft
r_sgm = la.diagm([0., 1., 1., 0.]) # (R)ight
b_sgm = la.diagm([1., 1., 0., 0.]) # (B)ottom
t_sgm = la.diagm([0., 0., 1., 1.]) # (T)op

# grab (b, i, j) indices lying on i-adjacent cells (sep. w/ $\hat{n} = \pm \hat{i}$)
get_x_adj(i::Int, j::Int) = begin
    return  [(2, i-1, j),
             (1, i+1, j),
             (4, i+1, j),
             (3, i-1, j)]
end
# ...                             j-adjacent cells (sep. w/ $\hat{n} = \pm \hat{j}$)
get_y_adj(i::Int, j::Int) = begin
    return [(4, i, j-1),
            (3, i, j-1),
            (2, i, j+1),
            (1, i, j+1)]
end

  module Std

  m_sf = (1. / 36.)
  m_cm = [ 4.  2.  1.  2.;
           2.  4.  2.  1.;
           1.  2.  4.  2.;
           2.  1.  2.  4.]
  m = m_sf * m_cm

  # Interior (G)radient matrix (L_{k})
  # We have (X) and (Y) variants cause $\hat{\Omega} \cdot L_{k} \psi_{k}$
  gx_sf = (1. / 12.)
  gx_cm = [ 2.  2.  1.  1.;
           -2. -2. -1. -1.;
           -1. -1. -2. -2.;
            1.  1.  2.  2.]
  gx = gx_sf * gx_cm

  gy_sf = (1. / 12.)
  gy_cm = [ 2.  1.  1.  2.;
            1.  2.  2.  1.;
           -1. -2. -2. -1.;
           -2. -1. -1. -2.]
  gy = gy_sf * gy_cm

  # (S)urface matrix (L_{k}^{\text{surf}})
  # again, (X) and (Y) since $\hat{\Omega} \cdot L_{k}^{surf} \psi_{k}^{surf}$
  sx_sf = (1. / 6.)
  sx_cm = [-2.  0.  0. -1.;
            0.  2.  1.  0.;
            0.  1.  2.  0.;
           -1.  0.  0. -2.]
  sx = sx_sf * sx_cm

  sy_sf = (1. / 6.)
  sy_cm = [-2. -1.  0.  0.;
           -1. -2.  0.  0.;
            0.  0.  2.  1.;
            0.  0.  1.  2.]
  sy = sy_sf * sy_cm

    module DSA

    # mass matrix
    m_sf = (1. / 36.)
    m_cm = [4. 2. 1. 2.;
            2. 4. 2. 1.;
            1. 2. 4. 2.;
            2. 1. 2. 4.]
    m = m_sf * m_cm

    # within-cell gradient
    gx_sf = (1. / 6.)
    gy_sf = (1. / 6.)

    gx_cm = [ 2. -2. -1.  1.;
             -2.  2.  1. -1.;
             -1.  1.  2. -2.;
              1. -1. -2.  2.]
    gy_cm = [ 2.  1. -1. -2.;
              1.  2. -2. -1.;
             -1. -2.  2.  1.;
             -2. -1.  1.  2.]

    gx = gx_sf * gx_cm
    gy = gy_sf * gy_cm

    # interior surface flux terms (face-wise)
    ifl_sf = (1. / 6.)
    ifb_sf = (1. / 6.)
    ifr_sf = (1. / 6.)
    ift_sf = (1. / 6.)

    ifl_cm = [2. 0. 0. 1.;
              0. 0. 0. 0.;
              0. 0. 0. 0.;
              1. 0. 0. 2.]
    ifb_cm = [2. 1. 0. 0.;
              1. 2. 0. 0.;
              0. 0. 0. 0.;
              0. 0. 0. 0.]
    ifr_cm = [0. 0. 0. 0.;
              0. 2. 1. 0.;
              0. 1. 2. 0.;
              0. 0. 0. 0.]
    ift_cm = [0. 0. 0. 0.;
              0. 0. 0. 0.;
              0. 0. 2. 1.;
              0. 0. 1. 2.]

    ifl = ifl_sf * ifl_cm
    ifb = ifb_sf * ifb_cm
    ifr = ifr_sf * ifr_cm
    ift = ift_sf * ift_cm

    # exterior surface flux terms (face-wise)
    efl_sf = (1. / 6.)
    efb_sf = (1. / 6.)
    efr_sf = (1. / 6.)
    eft_sf = (1. / 6.)

    efl_cm = [0. 2. 1. 0.;
              0. 0. 0. 0.;
              0. 0. 0. 0.;
              0. 1. 2. 0.]
    efb_cm = [0. 0. 1. 2.;
              0. 0. 2. 1.;
              0. 0. 0. 0.;
              0. 0. 0. 0.]
    efr_cm = [0. 0. 0. 0.;
              2. 0. 0. 1.;
              1. 0. 0. 2.;
              0. 0. 0. 0.]
    eft_cm = [0. 0. 0. 0.;
              0. 0. 0. 0.;
              1. 2. 0. 0.;
              2. 1. 0. 0.]

    efl = efl_sf * efl_cm
    efb = efb_sf * efb_cm
    efr = efr_sf * efr_cm
    eft = eft_sf * eft_cm

    # interior surface current terms (face-wise)
    icl_sf = (1. / 6.)
    icb_sf = (1. / 6.)
    icr_sf = (1. / 6.)
    ict_sf = (1. / 6.)

    icl_cm = [ 2. -2. -1.  1.;
               0.  0.  0.  0.;
               0.  0.  0.  0.;
               1. -1. -2.  2.]
    icb_cm = [ 2.  1. -1. -2.;
               1.  2. -2. -1.;
               0.  0.  0.  0.;
               0.  0.  0.  0.]
    icr_cm = [ 0.  0.  0.  0.;
              -2.  2.  1. -1.;
              -1.  1.  2. -2.;
               0.  0.  0.  0.]
    ict_cm = [ 0.  0.  0.  0.;
               0.  0.  0.  0.;
              -1. -2.  2.  1.;
              -2. -1.  1.  2.]

    icl = icl_sf * icl_cm
    icb = icb_sf * icb_cm
    icr = icr_sf * icr_cm
    ict = ict_sf * ict_cm

    # exterior surface current terms (face-wise)
    ecl_sf = (1. / 6.)
    ecb_sf = (1. / 6.)
    ecr_sf = (1. / 6.)
    ect_sf = (1. / 6.)

    ecl_cm = [ 2. -2. -1.  1.;
               0.  0.  0.  0.;
               0.  0.  0.  0.;
               1. -1. -2.  2.]
    ecb_cm = [ 2.  1. -1. -2.;
               1.  2. -2. -1.;
               0.  0.  0.  0.;
               0.  0.  0.  0.]
    ecr_cm = [ 0.  0.  0.  0.;
              -2.  2.  1. -1.;
              -1.  1.  2. -2.;
               0.  0.  0.  0.]
    ect_cm = [ 0.  0.  0.  0.;
               0.  0.  0.  0.;
              -1. -2.  2.  1.;
              -2. -1.  1.  2.]

    ecl = ecl_sf * ecl_cm
    ecb = ecb_sf * ecb_cm
    ecr = ecr_sf * ecr_cm
    ect = ect_sf * ect_cm
    end # module DSA
  end # module Std

  # NOTE : FLBLD
  module Lum

  m_sf = (1. / 4.)
  m_cm = [1. 0. 0. 0.; # identity
          0. 1. 0. 0.;
          0. 0. 1. 0.;
          0. 0. 0. 1.]
  m = m_sf * m_cm

  gx_sf = (1. / 4.)
  gx_cm = [ 1.  1.  0.  0.;
           -1. -1.  0.  0.;
            0.  0. -1. -1.;
            0.  0.  1.  1.]
  gx = gx_sf * gx_cm

  gy_sf = (1. / 4.)
  gy_cm = [ 1.  0.  0.  1.;
            0.  1.  1.  0.;
            0. -1. -1.  0.;
           -1.  0.  0. -1.]
  gy = gy_sf * gy_cm

  sx_sf = (1. / 2.)
  sx_cm = [-1.  0.  0.  0.;
            0.  1.  0.  0.;
            0.  0.  1.  0.;
            0.  0.  0. -1.]
  sx = sx_sf * sx_cm

  sy_sf = (1. / 2.)
  sy_cm = [-1.  0.  0.  0.;
            0. -1.  0.  0.;
            0.  0.  1.  0.;
            0.  0.  0.  1.]
  sy = sy_sf * sy_cm

  end # module Lum
end # module BLD
