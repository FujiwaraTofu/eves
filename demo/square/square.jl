# NOTE
# check BLD soln. convergence in the (t)hick (d)iffusion (l)imit
# 1 cm × 1 cm square
# vacuum boundary conditions
# asymptotically (ϵ) scaled, constant coefficients

# import the module
import Eves
import Eves.Utils

# use Eves defined type aliases
using Eves.Types

# import / use some external packages
import LinearAlgebra
import OffsetArrays
import Plots

using  LaTeXStrings

# module aliases
ev = Eves
ut = Utils
la = LinearAlgebra
oa = OffsetArrays
pl = Plots

# use PyPlot as our Plots backend
pl.pyplot()

sX = sY = 1.              # domain width & height (cm)
ni = nj = 20              # no. cells across
Δx, Δy = sX / ni, sY / nj # cell width & height (cm)
nn = 8                    # angular quadrature order

sq  = ev.SyQu(ev.gen_lqn_pr(nn)...) # symmetric angular quadrature
rm  = ev.ReMe(ni, nj, Δx, Δy)       # rectangular mesh
dtr = ev.dtr_bld_std                # transport discretization (BLD)
ddi = ev.ddi_asc_std                # diffusion discretization (Adams' semi-consistent)
bsp = ev.BoSp([ev.RB_R, ev.RB_T])   # reflective (R)ight & (T)op boundaries

nb = dtr.nb # no. basis funs. per cell
nm = sq.nm  # no. ordinates

# funs. to shift between data reps. (vectors for iteration, arrays for solvers)
sh_v2ϕ = ev.gen_sh_v2ϕ(rm, dtr)
sh_ϕ2v = ev.gen_sh_ϕ2v(rm, dtr)

# initial estimates
ϕs_i = zeros(nb, ni, nj)
# NOTE : for simple ghost-cell indexing, ψs are held in an OffsetArray
ψs_i = oa.OffsetArray(zeros(nb, nm, ni+2, nj+2), 1:nb, 1:nm, 0:(ni+1), 0:(nj+1))

# reshape ϕs_i for use in ev.fpi
vs_i, es_i = sh_ϕ2v(ϕs_i), ψs_i

mp = -4                      # (m)inimum (p)ower
δ  = 0.25                    # exponent step
ϵs = 10. .^ collect(mp:δ:0.) # coeff. scalings
nϵ = length(ϵs)

# indices into ϵs used for plotting
pleis = collect(1:Int(round(1 / δ)):length(ϵs))

# max no. iterations
mi = 50
# convergence function
cf = (o, n) -> la.norm(n - o, Inf) < 1.e-8

# base coefficients (these will be independently scaled by ϵ)
b_σt, b_σa, b_q = 1., 1., 1.

outs = []

for (i, ϵ) in enumerate(ϵs)
    println("ϵ = $(ϵ) [$(i) / $(nϵ)]")

    # this is the proper diffusion-limit coeff. scaling
    σt = b_σt / ϵ
    σa = b_σa * ϵ
    q  = b_q  * ϵ

    # create our material and inhomogeneous source
    ma = ev.Material(rm, σt, σa)
    qs = fill(q, nb, ni, nj)

    # generate DSA outer-iteration function
    dsa_fpf = ev.gen_outer_fpf(sq, ma, rm, bsp, qs; dtr = dtr, ddi = ddi, acc = ev.ACC_DSA)

    # use standard fixed-point-iteration
    fpi_tup = co, vs, es, vss = ev.fpi(dsa_fpf, vs_i, es_i; mi = mi, cf = cf, save = true)

    push!(outs, fpi_tup)
end

# evaluation mesh (Finite Element gives us a *function* repr. our soln.)
eni, enj = 200, 200
eΔx, eΔy = sX / eni, sY / enj
erm = ev.ReMe(eni, enj, eΔx, eΔy)
excs = ev.get_xcs(erm)
eycs = ev.get_ycs(erm)

# separate output into arrays
coa  = [o[1] for o in outs] # convergence flags
vssa = [o[4] for o in outs] # saved iteration vectors

# process the output arrays
ϕssa = [[sh_v2ϕ(vs) for vs in vss] for vss in vssa]             # reshape vss → ϕss
ϕsa  = [ϕss[end] for ϕss in ϕssa]                               # sols.
eϕsa = [ev.gen_sol_rep(rm, dtr, ϕs).(excs, eycs) for ϕs in ϕsa] # sols. eval. on erm
nita = [length(ϕss) for ϕss in ϕssa]                            # no. iterations
ersa = [[la.norm(ϕss[i] - ϕss[end], Inf)
         for i in 1:(length(ϕss) - 1)]
           for ϕss in ϕssa]                                     # iter. errors
ρsa = [(try ut.est_rho(ϕss) catch _ 0. end) for ϕss in ϕssa]   # spectral radii

# cell mean-free-path thicknesses (for each ϵ)
cms = [(b_σt / ϵ)*Δx for ϵ in ϵs]

# setup plotting directory
dir   = @__DIR__               # directory containing this script
pldir = joinpath(dir, "plots") # plots directory

# if the plots directory isn't there, make it
if ! isdir(pldir)
    mkdir(pldir)
end

# plotting results

# plot spectral radius (ρ) ...
# ... vs. diffusion limit scaling (ϵ)
pl_ϵρ = pl.plot(ϵs , ρsa, xscale = :log10, lw=2, legend=false, markershape=:auto,
                xlabel=L"\epsilon", ylabel = L"\rho")
# ... vs. cell mean-free-path thickness (σ_{t}Δx = σ_{t}Δy)
pl_mρ = pl.plot(cms, ρsa, xscale=:log10, lw=2, legend=false, markershape=:auto,
                xlabel = L"\sigma_{t}\Delta x = \sigma_{t}\Delta y", ylabel = L"\rho")

pl.savefig(pl_ϵρ, joinpath(pldir, "pl_eps_rho.pdf"))
pl.savefig(pl_mρ, joinpath(pldir, "pl_mfp_rho.pdf"))

# plot iterations to convergence ...
# ... vs. diffusion limit scaling (ϵ)
pl_ϵnit = pl.plot(ϵs , nita, xscale=:log10, lw=2, legend=false, markershape=:auto,
                  xlabel = L"\epsilon", ylabel = L"N")
# ... vs. cell mean-free-path thickness (σ_{t}Δx = σ_{t}Δy)
pl_mnit = pl.plot(cms, nita, xscale=:log10, lw=2, legend=false, markershape=:auto,
                  xlabel = L"\sigma_{t}\Delta x, \sigma_{t} \Delta y", ylabel = L"N")

# plot scalar flux solutions (contour)
pl_eϕs(ei) = begin
    pl_sol = pl.contour(excs[:,1], eycs[1,:], eϕsa[ei],
                           xlabel = L"$x$ (cm)", ylabel = L"$y$ (cm)",
                           colorbar_title = L"$\phi_{\epsilon = %$(ϵs[ei])}$ (cm$^{-2}$s$^{-1}$)")
    pl.savefig(pl_sol, joinpath(pldir, "pl_sol_ei_$(ϵs[ei]).pdf"))
end
[pl_eϕs(ei) for ei in pleis]

# plot scalar flux differences (contour)
pl_eds(ei1, ei2) = begin
    pl_dif = pl.contour(excs[:,1], eycs[1,:], log10.(abs.(eϕsa[ei1] - eϕsa[ei2])),
                        xlabel = L"$x$ (cm)", ylabel = L"$y$ (cm)",
                        colorbar_title = L"$\log_{10}(|\phi_{\epsilon = %$(ϵs[ei1])} - \phi_{\epsilon = %$(ϵs[ei2])}|)$ (cm$^{-2}$s$^{-1}$)")
    pl.savefig(pl_dif, joinpath(pldir, "pl_dif_ei_$(ϵs[ei1])_ei_$(ϵs[ei2]).pdf"))
end
[pl_eds(1, ei) for ei in pleis[2:end]]

