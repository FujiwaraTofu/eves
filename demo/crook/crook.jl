# NOTE
# crooked pipe problem parameters (to be shared among scripts)

import Eves

ev = Eves

using Eves.Types

import OffsetArrays

oa = OffsetArrays

# (c)rooked (p)ipe parameters
cp_X  = 10. # width  (cm)
cp_Y  =  6. # height (cm)

# check if a point is in the domain
cp_in_dom   = (x::Fl, y::Fl) -> begin
    return (0. < x < cp_X) && (0. < y < cp_Y)
end

# ... in the thin region?
cp_in_thin  = (x::Fl, y::Fl) -> begin
    if ! cp_in_dom(x,y)
        error()
    end
    return (   ((0. < x <  4.) && (0. < y < 2.))
            || ((3. < x <  7.) && (2. < y < 4.))
            || ((6. < x < 10.) && (0. < y < 2.)))
end

# ... in the thick region?
cp_in_thick = (x::Fl, y::Fl) -> begin
    return ! cp_in_thin(x, y) # NOTE : throws outside domain
end

cp_ψinc = (1. / (4. * π)) # left-boundary incident angular flux (isotropic)

# σt_thick : thick region total macroscopic cross section
# c        : scattering ratio (σs / σt) (0. ≤ c ≤ 1.)
# r        : (r)atio σt (thick) / σt (thin) (r ≥ 1.)
# nn       : LQN quadrature order (NOTE : nn != no. ordinates, in general)
# ni       : no. mesh cells (across width)
# nj       : ...            (across height)
gen_cp(σt_thick::Fl, c::Fl, r::Fl, nn::Int, ni::Int, nj::Int, dtr::ev.DFE_Tr) = begin
    # LQ_{N} (level-symmetric) quadrature set
    aq = ev.SyQu(ev.gen_lqn_pr(nn)...)
    nm = aq.nm # no. ordinates in quad. set

    Δx = cp_X / ni # cell width  (cm)
    Δy = cp_Y / nj # ...  height (cm)

    rm = ev.ReMe(ni, nj, Δx, Δy)

    # mesh cell-center's (x & y)
    xcs, ycs = ev.get_xcs(rm), ev.get_ycs(rm)

    # thin region total macroscopic cross section
    σt_thin = σt_thick / r # (cm^{-1})

    # gives the σt value at a given location
    σtf(x::Fl, y::Fl) = (cp_in_thick(x, y) ? σt_thick : σt_thin)

    # cell cross sections determined by cell-center position
    σts = σtf.(xcs, ycs)
    σas = (1. - c) .* σts # NOTE : domain-constant scattering ratio

    # construct material
    ma = ev.Material(rm, σts, σas)

    # construct internal inhomogeneous source (none)
    qs = zeros(dtr.nb, ni, nj)

    # generate initial estimates
    ϕs_i = zeros(dtr.nb, ni, nj)
    ψs_i = oa.OffsetArray(zeros(dtr.nb, nm, ni+2, nj+2), 1:dtr.nb, 1:nm, 0:(ni+1), 0:(nj+1))
    # NOTE : we still need to set incident flux values on ψs_i, we do this next

    # boundary condition specification

    # left-incident
    ghbs = [2,3] # incident basis points on left-boundary ghost cells
    ms_rg = [ev.get_ms(aq, ev.QC_RU); ev.get_ms(aq, ev.QC_RD)]
    ψs_i[ghbs, ms_rg, 0, 1:nj] .= cp_ψinc

    # bottom-reflective set in BoundSpec
    bsp = ev.BoSp([ev.RB_B])

    return aq, ma, rm, bsp, qs, ϕs_i, ψs_i
end
