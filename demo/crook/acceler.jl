# NOTE
# solve the crooked pipe problem using Diffusion Synthetic Acceleration (DSA)
# using standard Fixed-Point Iteration (FPI). Also, explore the use of iterative
# accelerators like Dynamic Mode Decomposition (DMD), and Anderson acceleration

# include crooked pipe utilities
include("crook.jl")

import Eves
using  Eves.Types

ev = Eves

import LinearAlgebra
import Plots
using  LaTeXStrings

la = LinearAlgebra
pl = Plots

pl.pyplot() # use PyPlot backend

# script parameters (user-variable)
σt_thick = 100.           # thick region macroscopic total XS
c        =   1.           # scattering ratio (pure scattering)
nn       =   8            # LQN ang. quadrature order
ni       =  20            # no. cells across system width
nj       =  12            # ...                     height
dtr      = ev.dtr_bld_std # tran. disc. (standard, un-lumped BLD)
ddi      = ev.ddi_asc_std # diff. disc. (Adams' semi-consistent BLD)
ϵ        = 1.e-6          # convergence criteria (standard)
ϵr       = 1.e-2 * ϵ      # ...                  (reference)
mi       =  50            # maximum no. iters.   (standard)
mir      = 100            # ...                  (reference)

# quantities we wish to vary
# thick-to-thin density ratios
rs = [1, 5, 10, 25, 50, 75, 100, 250, 500, 1_000, 2_500, 5_000, 10_000, 100_000]
R  = 0              # DMD, no. smoothing iterations (fixed, for now)
Ls = [2, 4, 10, 20] # DMD, snapshot matrix no. cols.
ms = [2, 4, 10, 20] # Anderson acc., storage length

# convergence function (reference)
cfr = (o, n) -> (la.norm(n - o, Inf) < ϵr)

# setup directory for data storage
dir = @__DIR__                  # current directory
datdir = joinpath(dir, "data")  # data directory
pltdir = joinpath(dir, "plots") # plots directory

# if the data  directory isn't there, make it
(! isdir(datdir)) ? mkdir(datdir) : nothing
# ...    plots ...
(! isdir(pltdir)) ? mkdir(pltdir) : nothing

hdf_file = joinpath(datdir, "acceler.h5")

# forcibly move the h5 file if it already exists
if isfile(hdf_file)
    # NOTE : Julia uses (*) for string concatenation for some asinine reason
    mv(hdf_file, hdf_file * ".old"; force = true)
end

# loop sizes
eI  = length(rs) # outer
eJd = length(Ls) # inner (DMD)
eJa = length(ms) # inner (Anderson)

# initialize output arrays
re_outs = Array{Any,1}(undef, eI)      # reference
fp_outs = Array{Any,1}(undef, eI)      # fixed-point-iteration
dm_outs = Array{Any,2}(undef, eI, eJd) # DMD
an_outs = Array{Any,2}(undef, eI, eJa) # Anderson

# shift between data representations (array (solver) vs. vector (iteration))
sh_v2ϕ(vs::VFl)       = reshape(vs, dtr.nb, ni, nj)   # iteration → solver
sh_ϕ2v(ϕs::Arr{Fl,3}) = reshape(ϕs, dtr.nb * ni * nj) # solver    → iteration

for (ei, r) in enumerate(rs)
    ei_str = "r = $(r) [i = $(ei)/$(eI)]"
    println(ei_str)

    # gen. the crooked-pipe problem for the current thick-to-thin ratio (r)
    aq, ma, rm, bsp, qs, ϕs_i, ψs_i = gen_cp(σt_thick, c, Fl(r), nn, ni, nj, dtr)

    # gen. the DSA outer-iteration fixed-point-function (fpf)
    dsa = ev.gen_outer_fpf(aq, ma, rm, bsp, qs; dtr = dtr, ddi = ddi)

    # reshape / set our initial estimates
    vs_i, es_i = sh_ϕ2v(ϕs_i), ψs_i

    # reference solution (obtained using fixed-point-iteration)
    println("\tReference")
    re_co, re_vs, re_es, re_vss = re_tup = ev.fpi(dsa, vs_i, es_i; mi = mir, cf = cfr, save = false)
    @assert re_co         # assert the reference soln. is converged
    re_outs[ei] = re_tup  # saving loop output

    # for FPI, DMD, and Anderson we check convergence based on the reference soln.
    cf = (_ovs, nvs) -> (la.norm(nvs - re_vs, Inf) < ϵ)

    # fixed-point iteration (TODO : this is pretty redundant w/ the reference calculation)
    println("\tFPI")
    fp_tup = ev.fpi(dsa, vs_i, es_i; mi = mi, cf = cf, save = true)
    fp_outs[ei] = fp_tup

    # dynamic-mode-decomposition accelerated fixed-point-iteration (vary L)
    println("\tDMD")
    for (ejd, L) in enumerate(Ls)
        ejd_str = "\t\tL = $(L), [jd = $(ejd)/$(eJd)]"
        println(ejd_str)
        dm_tup = ev.dmd(dsa, R, L, vs_i, es_i;
                        coarsen = ev.moot_coarsen, # no coarsening
                        interpo = ev.moot_interpo, # no interpolation
                        mi = mi, cf = cf, save = true)
        dm_outs[ei, ejd] = dm_tup
    end

    # Anderson accelerated fixed-point-iteration (vary m)
    println("\tAnderson")
    for (eja, m) in enumerate(ms)
        eja_str = "\t\tm = $(m), [ja = $(eja)/$(eJa)]"
        println(eja_str)
        an_tup = ev.and(dsa, m, vs_i, es_i;
                        coarsen = ev.moot_coarsen, # no coarsening
                        interpo = ev.moot_interpo, # no interpolation
                        mi = mi, cf = cf, save = true)
        an_outs[ei, eja] = an_tup
    end
end

# evaluation mesh (Finite Element gives us a *function* repr. our soln.)
eni, enj = 300, 180
eΔx, eΔy = cp_X / eni, cp_Y / enj
erm = ev.ReMe(eni, enj, eΔx, eΔy)
excs = ev.get_xcs(erm)
eycs = ev.get_ycs(erm)

# functions to extract information from output tuples (fp_tup, dm_tup, ...)
# no. iterations
ext_nit(out_tup) = (vss = out_tup[4]; nit = length(vss); nit)

# scalar flux soln. evaluated on erm
rm = ev.ReMe(ni, nj, cp_X / ni, cp_Y / nj) # mesh used in the data coll. loop
ext_eϕs(out_tup) = begin
    vs  = out_tup[2] # soln. vector
    ϕs  = sh_v2ϕ(vs) # soln. array
    eϕs = ev.gen_sol_rep(rm, dtr, ϕs).(excs, eycs)
    return eϕs
end

fp_nits = [ext_nit(o) for o in fp_outs]
dm_nits = [ext_nit(o) for o in dm_outs]
an_nits = [ext_nit(o) for o in an_outs]

re_eϕs = [ext_eϕs(o) for o in re_outs]

# plot iterations to convergence
# legend labels
labs_fp = ["DSA"]
labs_dm = ["DMD (L=$(L))" for L in Ls]
labs_an = ["Anderson (m=$(m))" for m in ms]
labs = reshape([labs_fp; labs_dm; labs_an], 1, 1 + length(Ls) + length(ms))
pl_nits = pl.plot(rs, [fp_nits dm_nits an_nits], lw=2, legend=:topleft, labels=labs,
                  xscale=:log10, markershape=:auto, xlabel=L"\epsilon", ylabel=L"N")
pl.savefig(pl_nits, joinpath(pltdir, "pl_nits.pdf"))

# plots erm-evaluated reference solution contours
for (ei, r) in enumerate(rs)
    pl_eϕs = pl.contour(excs[:,1], eycs[1,:], permutedims(re_eϕs[ei]),
                        xlabel = L"$x$ (cm)", ylabel = L"$y$ (cm)",
                        colorbar_title = L"$\phi_{r=%$(r)}$ (cm$^{-2}$s$^{-1}$)")
    pl.savefig(pl_eϕs, joinpath(pltdir, "pl_ephis_r=$(r).pdf"))
end
